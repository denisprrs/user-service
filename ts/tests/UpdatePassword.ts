import "mocha";
import { fail, equal, deepEqual } from "assert";
import * as mockito from "ts-mockito";
import { Database } from "../src/database/Database";
import * as sinon from "sinon";
import ServiceError from "../src/extend/ServiceError";
import UpdatePassword from "../src/helpers/UpdatePassword";
import { HashPassword } from "../src/helpers/HashPassword";
import { HashedPassword } from "../src/interfaces/HashedPassword";

describe("Test Update password", () => {
    let dependencyMock: any;
    let database: Database;

    before(() => {
        database = mockito.mock(Database);
        dependencyMock = {
            database: database
        };
    });

    it("should throw error if user not found", async() => {
        database.getUserById = sinon.stub().returns(Promise.resolve(null));

        try {
            await UpdatePassword("userId", "oldPass", "newPass", dependencyMock);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("User not found", 404, "userNotFound"));
        }
    });

    it("should throw error if old password does not match", async() => {
        let password: string = "SomePassword123!";
        let hashedPassword: HashedPassword = await HashPassword(password);

        let user: any = {
            id: "userId",
            username: "username",
            salt: hashedPassword.salt,
            password: hashedPassword.password
        };

        database.getUserById = sinon.stub().returns(Promise.resolve(user));

        try {
            await UpdatePassword("userId", "oldPass", "newPass", dependencyMock);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("Old and new password does not match", 400, "passwordsNotMatching"));
        }
    });

    it("should throw error if old password does not match", async() => {
        let password: string = "SomePassword123!";
        let password2: string = "SomePassword122!";
        let hashedPassword: HashedPassword = await HashPassword(password);

        let user: any = {
            id: "userId",
            username: "username",
            salt: hashedPassword.salt,
            password: hashedPassword.password
        };

        database.getUserById = sinon.stub().returns(Promise.resolve(user));
        database.updateUser = sinon.stub().returns(Promise.resolve());

        try {
            await UpdatePassword("userId", password, password2, dependencyMock);
        } catch (e) {
            fail(e);
        }
    });
});
