import "mocha";
import { fail, equal, deepEqual } from "assert";
import GetAuthToken from "../src/helpers/GetAuthToken";
import { AuthToken } from "../src/interfaces/AuthToken";
import ValidateToken from "../src/helpers/ValidateToken";
import ServiceError from "../src/extend/ServiceError";
import { Token } from "../src/interfaces/Token";

describe("Create JWT signed token", () => {
    it("should create and sign token", async() => {
        let user: any = {
            username: "somename",
            password: "password",
            salt: "salt",
            _id: "someId"
        };
        try {
            let tokenData: AuthToken = GetAuthToken(user, "SecretKey");

            equal(tokenData.expiresIn, 3600);
            equal(typeof tokenData.token, "string");
        } catch (e) {
            fail(e);
        }
    });

    it("should create and sign token and then validate it", async() => {
        let user: any = {
            username: "somename",
            password: "password",
            salt: "salt",
            _id: "someId"
        };
        try {
            let tokenData: AuthToken = GetAuthToken(user, "SecretKey");

            equal(tokenData.expiresIn, 3600);
            equal(typeof tokenData.token, "string");

            let data: Token = ValidateToken(tokenData.token, "SecretKey");
        } catch (e) {
            fail(e);
        }
    });

    it("should create and sign token and then validate it", async() => {
        let user: any = {
            username: "somename",
            password: "password",
            salt: "salt",
            _id: "someId"
        };
        try {
            let tokenData: AuthToken = GetAuthToken(user, "SecretKey");

            equal(tokenData.expiresIn, 3600);
            equal(typeof tokenData.token, "string");

            ValidateToken(tokenData.token, "SecretKeyy");
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("Token is invalid", 401, "invalidToken"));
        }
    });
});
