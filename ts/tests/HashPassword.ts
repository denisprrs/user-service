import "mocha";
import { HashPassword } from "../src/helpers/HashPassword";
import { HashedPassword } from "../src/interfaces/HashedPassword";
import { fail, equal } from "assert";

describe("Hash password", () => {
    it("should get salt and password", async() => {
        let password: string = "Password123";
        try {
            let hashedPassword: HashedPassword = await HashPassword(password);
            equal(typeof hashedPassword.salt === "string", true, hashedPassword.salt);
            equal(typeof hashedPassword.password === "string", true, hashedPassword.password);
        } catch (e) {
            fail(e);
        }
    });

    it("should get same hashed string if same salt and password", async() => {
        let password: string = "Password123";
        try {
            // Generate salt
            let hashedPassword: HashedPassword = await HashPassword(password);
            equal(typeof hashedPassword.salt === "string", true, hashedPassword.salt);
            equal(typeof hashedPassword.password === "string", true, hashedPassword.password);

            // Generate hashed string from same salt and password
            let hashedPassword2: HashedPassword = await HashPassword(password, hashedPassword.salt);
            equal(typeof hashedPassword.salt === "string", true, hashedPassword2.salt);
            equal(typeof hashedPassword.password === "string", true, hashedPassword2.password);

            equal(hashedPassword2.password === hashedPassword.password, true, hashedPassword2.password);
        } catch (e) {
            fail(e);
        }
    });
});
