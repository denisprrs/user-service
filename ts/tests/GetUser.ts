import "mocha";
import { fail, equal, deepEqual } from "assert";
import * as mockito from "ts-mockito";
import { Database } from "../src/database/Database";
import * as sinon from "sinon";
import ServiceError from "../src/extend/ServiceError";
import GetUser from "../src/helpers/GetUser";
import { UserMe } from "ts/src/interfaces/UserMe";

describe("Test Get User", () => {
    let dependencyMock: any;
    let database: Database;

    before(() => {
        database = mockito.mock(Database);
        dependencyMock = {
            database: database
        };
    });

    it("should throw error if user not found", async() => {
        database.getUserById = sinon.stub().withArgs("userId").returns(Promise.resolve(null));
        try {
            await GetUser("userId", dependencyMock);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("User not found", 404, "userNotFound"));
        }
    });

    it("should return user", async() => {
        let user: any = {
            username: "username",
            id: "id"
        };

        let expected: any = {
            username: "username",
            id: "id"
        };

        database.getUserById = sinon.stub().withArgs("userId").returns(Promise.resolve(user));

        try {
            let userMe: UserMe = await GetUser("userId", dependencyMock);
            deepEqual(userMe, expected, JSON.stringify(userMe));
        } catch (e) {
            fail(e);
        }
    });
});
