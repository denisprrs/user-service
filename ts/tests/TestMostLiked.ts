import "mocha";
import { fail, equal, deepEqual } from "assert";
import * as mockito from "ts-mockito";
import { Database } from "../src/database/Database";
import * as sinon from "sinon";
import GetMostLikedUsers from "../src/helpers/GetMostLikedUsers";
import { UserMostLiked } from "../src/interfaces/UserMostLiked";

describe("Test Most Liked", () => {
    let dependencyMock: any;
    let database: Database;

    before(() => {
        database = mockito.mock(Database);
        dependencyMock = {
            database: database
        };
    });

    it("should return empty array", async() => {
        database.getUsers = sinon.stub().returns(Promise.resolve(null));
        database.getLikes = sinon.stub().returns(Promise.resolve(null));

        try {
            let likedUsers: UserMostLiked[] = await GetMostLikedUsers(dependencyMock);
            deepEqual(likedUsers, [], JSON.stringify(likedUsers));
        } catch (e) {
            fail(e);
        }
    });

    it("should return sorted array with two items", async() => {
        let users: any = [{
            username: "username1",
            id: "id1"
        }, {
            username: "username2",
            id: "id2"
        }];

        let likes: any = [{
            userId: "id1",
            id: "id1"
        }, {
            userId: "id1",
            id: "id2"
        }];

        let expected: any = [{
            username: "username1",
            id: "id1",
            likes: 2
        }, {
            username: "username2",
            id: "id2",
            likes: 0
        }];

        database.getUsers = sinon.stub().returns(Promise.resolve(users));
        database.getLikes = sinon.stub().returns(Promise.resolve(likes));

        try {
            let likedUsers: UserMostLiked[] = await GetMostLikedUsers(dependencyMock);
            deepEqual(likedUsers, expected, JSON.stringify(likedUsers));
        } catch (e) {
            fail(e);
        }
    });
});
