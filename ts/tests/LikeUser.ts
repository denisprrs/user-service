import "mocha";
import { fail, equal, deepEqual } from "assert";
import * as mockito from "ts-mockito";
import { Database } from "../src/database/Database";
import * as sinon from "sinon";
import ServiceError from "../src/extend/ServiceError";
import LikeUser from "../src/helpers/LikeUser";

describe("Test Like user", () => {
    let dependencyMock: any;
    let database: Database;

    before(() => {
        database = mockito.mock(Database);
        dependencyMock = {
            database: database
        };
    });

    it("should throw error if user tries to like himself", async() => {
        try {
            await LikeUser("userId", "userId", dependencyMock);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("You obviously cannot like yourself, you narcissist.", 400, "cannotLikeYourSelf"));
        }
    });

    it("should throw error if user not found", async() => {
        database.getUserById = sinon.stub().withArgs("likedUserId").returns(Promise.resolve(null));

        try {
            await LikeUser("userId", "likedUserId", dependencyMock);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("User not found", 404, "userNotFound"));
        }
    });

    it("should throw error if same record already exits", async() => {
        let user: any = {
            id: "userId",
            username: "username"
        };

        let error: any = new Error("duplicate key error collection");
        error.name = "MongoError";

        database.getUserById = sinon.stub().withArgs("likedUserId").returns(Promise.resolve(user));
        database.createLike = sinon.stub().returns(Promise.reject(error));

        try {
            await LikeUser("userId", "likedUserId", dependencyMock);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("You already liked this user", 400, "alreadyLiked"));
        }
    });


    it("should create record for like", async() => {
        let user: any = {
            id: "userId",
            username: "username"
        };

        database.getUserById = sinon.stub().withArgs("likedUserId").returns(Promise.resolve(user));
        database.createLike = sinon.stub().returns(Promise.resolve());

        try {
            await LikeUser("userId", "likedUserId", dependencyMock);
        } catch (e) {
            fail(e);
        }
    });
});
