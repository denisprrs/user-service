import "mocha";
import { fail, equal, deepEqual } from "assert";
import * as mockito from "ts-mockito";
import { Database } from "../src/database/Database";
import * as sinon from "sinon";
import UnlikeUser from "../src/helpers/UnLikeUser";
import ServiceError from "../src/extend/ServiceError";

describe("Test Unlike user", () => {
    let dependencyMock: any;
    let database: Database;

    before(() => {
        database = mockito.mock(Database);
        dependencyMock = {
            database: database
        };
    });

    it("should throw error if user not found", async() => {
        database.getLike = sinon.stub().returns(Promise.resolve(null));

        try {
            await UnlikeUser("userId", "unlikeUserId", dependencyMock);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("Like not found", 404, "likeNotFound"));
        }
    });

    it("should delete like", async() => {
        let user: any = {
            id: "userId",
            username: "username"
        };

        database.getLike = sinon.stub().withArgs("userId", "unlikeUserId").returns(Promise.resolve(user));

        try {
            await UnlikeUser("userId", "unlikeUserId", dependencyMock);
        } catch (e) {
            fail(e);
        }
    });
});
