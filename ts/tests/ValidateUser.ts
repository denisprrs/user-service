import "mocha";
import { fail, equal, deepEqual } from "assert";
import { ValidateUser } from "../src/validators/ValidateUser";
import ServiceError from "../src/extend/ServiceError";

describe("Validate user", () => {
    it("should throw error if missing password", async() => {
        let user: any = {
            username: "username"
        };
        try {
            ValidateUser(user);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("Invalid property 'password'", 400, "invalidData"));
        }
    });

    it("should throw error if missing username", async() => {
        let user: any = {
            password: "password"
        };
        try {
            ValidateUser(user);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("Invalid property 'username'", 400, "invalidData"));
        }
    });

    it("should pass validation", async() => {
        let user: any = {
            username: "username",
            password: "password",
            extra: "extra"
        };
        try {
            let validatedUser: any = ValidateUser(user);
            deepEqual(validatedUser, {
                username: "username",
                password: "password"
            });
        } catch (e) {
            fail(e);
        }
    });
});
