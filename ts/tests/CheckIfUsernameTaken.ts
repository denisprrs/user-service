import "mocha";
import { fail, equal } from "assert";
import { Database } from "../src/database/Database";
import * as mockito from "ts-mockito";
import CheckIfUsernameTaken from "../src/helpers/CheckIfUsernameTaken";
import * as sinon from "sinon";

describe("Check if user already taken", () => {
    let database: Database;

    before(() => {
        database = mockito.mock(Database);
    });

    it("should return false if username not taken", async() => {
        database.getUserByUsername = sinon.stub().returns(Promise.resolve(null));

        try {
            let status: boolean = await CheckIfUsernameTaken("username", database);
            equal(status, false);
        } catch (e) {
            fail(e);
        }
    });

    it("should return true if username taken", async() => {
        database.getUserByUsername = sinon.stub().returns(Promise.resolve({ username: "someUsername"}));

        try {
            let status: boolean = await CheckIfUsernameTaken("username", database);
            equal(status, true);
        } catch (e) {
            fail(e);
        }
    });
});
