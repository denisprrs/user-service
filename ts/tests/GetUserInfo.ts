import "mocha";
import { fail, equal, deepEqual } from "assert";
import * as mockito from "ts-mockito";
import { Database } from "../src/database/Database";
import * as sinon from "sinon";
import ServiceError from "../src/extend/ServiceError";
import GetUserInfo from "../src/helpers/GetUserInfo";
import { PublicUser } from "../src/interfaces/PublicUser";

describe("Test Get User Info", () => {
    let dependencyMock: any;
    let database: Database;

    before(() => {
        database = mockito.mock(Database);
        dependencyMock = {
            database: database
        };
    });

    it("should throw error if user not found", async() => {
        database.getUserById = sinon.stub().withArgs("userId").returns(Promise.resolve(null));
        try {
            await GetUserInfo("userId", dependencyMock);
            fail("Should not pass");
        } catch (e) {
            deepEqual(e, new ServiceError("User not found", 404, "userNotFound"));
        }
    });

    it("should return zero likes if one not found", async() => {
        let user: any = {
            id: "userId",
            username: "username"
        };

        database.getUserById = sinon.stub().withArgs("userId").returns(Promise.resolve(user));
        database.getLikesForUser = sinon.stub().withArgs("userId").returns(Promise.resolve(null));

        try {
            let publicUser: PublicUser = await GetUserInfo("userId", dependencyMock);
            deepEqual(publicUser, { username: "username", likes: 0 });
        } catch (e) {
            fail(e);
        }
    });

    it("should return zero likes if one not found", async() => {
        let user: any = {
            id: "userId",
            username: "username"
        };

        let likes: any = [{
            userId: "userId"
        }];

        database.getUserById = sinon.stub().withArgs("userId").returns(Promise.resolve(user));
        database.getLikesForUser = sinon.stub().withArgs("userId").returns(Promise.resolve(likes));

        try {
            let publicUser: PublicUser = await GetUserInfo("userId", dependencyMock);
            deepEqual(publicUser, { username: "username", likes: 1 });
        } catch (e) {
            fail(e);
        }
    });
});
