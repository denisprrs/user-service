import { BaseMongoRepository } from "./BaseMongoRepository";
import UserSchema from "../schemas/UserSchema";
import { UserDocument } from "../interfaces/UserDocument";

export class UserMongoRepository extends BaseMongoRepository<UserDocument> {
    constructor() {
        super(UserSchema);
    }
}

Object.seal(UserMongoRepository);
