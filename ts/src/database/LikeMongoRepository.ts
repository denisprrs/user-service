import { BaseMongoRepository } from "./BaseMongoRepository";
import { LikeDocument } from "../interfaces/LikeDocument";
import LikeDocumentSchema from "../schemas/LikeSchema";

export class LikeMongoRepository extends BaseMongoRepository<LikeDocument> {
    constructor() {
        super(LikeDocumentSchema);
    }
}

Object.seal(LikeMongoRepository);
