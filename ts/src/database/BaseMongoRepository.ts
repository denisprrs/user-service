import { Document, Model, Types } from "mongoose";
import { Read } from "../interfaces/Read";
import { Write } from "../interfaces/Write";

export class BaseMongoRepository<T extends Document> implements Read<T>, Write<T> {

    private _model: Model<Document>;

    constructor(schemaModel: Model<Document>) {
        this._model = schemaModel;
    }

    create(item: T): Promise<T> {
        return <Promise<T>>this._model.create(item);
    }

    retrieveByType(type: string): Promise<T[]> {
        return <Promise<T[]>>this._model.find({type: type}).exec();
    }

    find(cond?: Object, options?: Object): Promise<T[]> {
        return <Promise<T[]>>this._model.find(cond, options).exec();
    }

    findOne(cond?: Object): Promise<T> {
        return <Promise<T>>this._model.findOne(cond).exec();
    }

    update(_id: Types.ObjectId, item: T): Promise<T> {
        return <Promise<T>>this._model.update({ _id: _id }, item).exec();
    }

    delete(_id: Types.ObjectId): Promise<void> {
        return this._model.remove({ _id: _id }).exec();
    }
}
