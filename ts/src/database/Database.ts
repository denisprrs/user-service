
import { User } from "../interfaces/User";
import { UserMongoRepository } from "../database/UserMongoRepository";
import { connect, Types } from "mongoose";
import { UserDocument } from "../interfaces/UserDocument";
import { LikeMongoRepository } from "./LikeMongoRepository";
import { LikeDocument } from "../interfaces/LikeDocument";

export class Database {
    private config: any;
    private userRepository: UserMongoRepository;
    private likeRepository: LikeMongoRepository;

    constructor(config: any, userRepository: UserMongoRepository, likeRepository: LikeMongoRepository) {
        this.config = config;
        this.userRepository = userRepository;
        this.likeRepository = likeRepository;
    }

    public async initialize(): Promise<void> {
        await connect(this.config.uri, { useNewUrlParser: true });
    }

    /**
     * Create new user
     *
     * @param user User
     */
    public async createUser(user: UserDocument): Promise<UserDocument> {
        user.type = "user";
        return await this.userRepository.create(user);
    }

    /**
     * Gets user by username and password
     *
     * @param username String
     * @param password String
     */
    public async getUser(username: string, password: string): Promise<UserDocument> {
        return await this.userRepository.findOne({
            username: username,
            password: password,
            type: "user"
        });
    }

    /**
     * Searches user by its username and returns it
     *
     * @param username String
     */
    public async getUserByUsername(username: string): Promise<UserDocument> {
        return await this.userRepository.findOne({
            username: username,
            type: "user"
        });
    }

    /**
     * Searches user by its id and returns it
     *
     * @param id String
     */
    public async getUserById(id: string): Promise<UserDocument> {
        return await this.userRepository.findOne({
            _id: id,
            type: "user"
        });
    }

    /**
     * Update user data
     *
     * @param user UserDocument
     */
    public async updateUser(user: UserDocument): Promise<UserDocument> {
        return await this.userRepository.update(user._id, user);
    }

    /**
     * Create new link
     *
     * @param like LikeDocument
     */
    public async createLike(like: LikeDocument): Promise<LikeDocument> {
        like.type = "like";
        return await this.likeRepository.create(like);
    }

    /**
     * Retrieves like
     *
     * @param userId String
     * @param likedUserId String
     */
    public async getLike(userId: string, likedUserId: string): Promise<LikeDocument | null> {
        return await this.likeRepository.findOne({
            userId: likedUserId,
            givenBy: userId,
            type: "like"
        });
    }

    /**
     * Delete like
     *
     * @param like LikeDocument
     */
    public async deleteLike(like: LikeDocument): Promise<void> {
        await this.likeRepository.delete(like._id);
    }

    /**
     * Returns all likes for user
     *
     * @param userId String
     */
    public async getLikesForUser(userId: string): Promise<LikeDocument[]> {
        return await this.likeRepository.find({
            userId: userId,
            type: "like"
        });
    }

    /**
     * Returns all users
     */
    public async getUsers(): Promise<UserDocument[]> {
        return await this.userRepository.find({
            type: "user"
        });
    }

    /**
     * Returns all likes
     */
    public async getLikes(): Promise<LikeDocument[]> {
        return await this.likeRepository.find({
            type: "like"
        });
    }
}
