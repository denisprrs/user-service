import { UseBefore } from "@tsed/common";
import { Store } from "@tsed/core";
import SecurityTokenMiddleware from "../middlewares/SecurityTokenMiddleware";


/**
 * Authorizes endpoint with given scope. Sets token to request (CTRequest)
 * @param scopes endpoint target scopes
 */
export function SecurityToken(): Function {
    return Store.decorate((store: Store) => {
        return UseBefore(SecurityTokenMiddleware);
    });
}
