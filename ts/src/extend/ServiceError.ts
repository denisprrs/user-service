
export default class ServiceError extends Error {
    public status: number;
    public errorCode: string;

    constructor(message: string, status: number, errorCode: string) {
        super(message);

        Error.captureStackTrace(this, this.constructor);

        this.status = status || 500;
        this.errorCode = errorCode || "internalServerError";
        this.message = message || "Unexpected internal server error.";
    }
}
