import { Name, Description, Returns, Security } from "@tsed/swagger";
import { Controller, Post, Next, Res, Req, BodyParams, Get, Put, Delete, PathParams, Service } from "@tsed/common";
import DependencyService from "../services/DependecyService";
import * as express from "express";
import { UserDocument } from "../interfaces/UserDocument";
import { SecurityToken } from "../decorators/SecurityToken";
import { TokenRequest } from "../interfaces/TokenRequest";
import { UpdatePasswordPayloadModel } from "../swagger/models/UpdatePasswordPayloadModel";
import { UserMostLiked } from "../interfaces/UserMostLiked";
import GetMostLikedUsers from "../helpers/GetMostLikedUsers";
import { MostLikedUsersResponseModel } from "../swagger/models/MostLikedUsersResponseModel";
import UnlikeUser from "../helpers/UnLikeUser";
import LikeUser from "../helpers/LikeUser";
import { PublicUser } from "../interfaces/PublicUser";
import GetUser from "../helpers/GetUser";
import UpdatePassword from "../helpers/UpdatePassword";
import GetUserInfo from "../helpers/GetUserInfo";
import { UserMe } from "../interfaces/UserMe";

@Name("")
@Controller("")
export class UserController {

    constructor(private dependencies: DependencyService) {
        //
    }

    @Get("/me")
    @SecurityToken()
    @Security("Bearer")
    @Description("Returns user profile data.")
    @Returns(200, { description: "User profile" })
    public async Me(
        @Req() request: TokenRequest,
        @Res() response: express.Response,
        @Next() next: express.NextFunction): Promise<void> {
            try {
                let userMe: UserMe = await GetUser(request.token.sub, this.dependencies);
                response.json(userMe);
            } catch (e) {
                next(e);
            }
    }

    @Put("/me/update-password")
    @SecurityToken()
    @Security("Bearer")
    @Description("Update old password with new one.")
    @Returns(200, { description: "Success" })
    public async UpdatePassword(
        @Req() request: TokenRequest,
        @Res() response: express.Response,
        @Next() next: express.NextFunction,
        @BodyParams() body: UpdatePasswordPayloadModel): Promise<void> {
            try {
                await UpdatePassword(request.token.sub, body.oldPassword, body.newPassword, this.dependencies);
                response.end();
            } catch (e) {
                next(e);
            }
    }

    @Get("/user/:id")
    @Description("Returns users and likes that they got.")
    @Returns(200, { description: "Users with likes" })
    public async UsersLikes(
        @PathParams("id") id: string,
        @Res() response: express.Response,
        @Next() next: express.NextFunction): Promise<void> {

            try {
                let publicUser: PublicUser = await GetUserInfo(id, this.dependencies);
                response.json(publicUser);
            } catch (e) {
                next(e);
            }
    }

    @Post("/user/:id/like")
    @SecurityToken()
    @Security("Bearer")
    @Description("Marks user as liked.")
    @Returns(200, { description: "User liked." })
    public async LikeUser(
        @PathParams("id") id: string,
        @Req() request: TokenRequest,
        @Res() response: express.Response,
        @Next() next: express.NextFunction): Promise<void> {
            try {
                await LikeUser(request.token.sub, id, this.dependencies);
                response.end();
            } catch (e) {
                next(e);
            }
    }

    @Delete("/user/:id/unlike")
    @SecurityToken()
    @Security("Bearer")
    @Description("Unlike user.")
    @Returns(200, { description: "Like deleted" })
    public async UnlikeUser(
        @Req() request: TokenRequest,
        @PathParams("id") id: string,
        @Res() response: express.Response,
        @Next() next: express.NextFunction): Promise<void> {
            try {
                await UnlikeUser(request.token.sub, id, this.dependencies);
                response.end();
            } catch (e) {
                next(e);
            }
    }

    @Get("/most-liked")
    @Description("Returns most liked users.")
    @Returns(200, { description: "Most liked users", type: MostLikedUsersResponseModel })
    public async MostLikedUsers(
        @Res() response: express.Response,
        @Next() next: express.NextFunction): Promise<void> {
            try {
                const mostLiked: UserMostLiked[] = await GetMostLikedUsers(this.dependencies);
                response.json(mostLiked);
            } catch (e) {
                next(e);
            }
    }
}
