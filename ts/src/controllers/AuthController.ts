import { Name, Description, Returns } from "@tsed/swagger";
import { Controller, Post, Next, Res, Req, BodyParams } from "@tsed/common";
import DependencyService from "../services/DependecyService";
import { User } from "../interfaces/User";
import * as express from "express";
import { SignUpUserModel } from "../swagger/models/SignUpUserModel";
import { ValidateUser } from "../validators/ValidateUser";
import { UserDocument } from "../interfaces/UserDocument";
import { UserCreatedResponseModel } from "../swagger/models/UserCreatedResponseModel";
import CheckIfUsernameTaken from "../helpers/CheckIfUsernameTaken";
import ServiceError from "../extend/ServiceError";
import { HashedPassword } from "../interfaces/HashedPassword";
import { HashPassword } from "../helpers/HashPassword";
import { LoginRequestModel } from "../swagger/models/LoginRequestModel";
import LoginUser from "../helpers/LoginUser";
import GetAuthToken from "../helpers/GetAuthToken";
import { AuthToken } from "../interfaces/AuthToken";

@Name("")
@Controller("")
export class AuthController {

    constructor(private dependencies: DependencyService) {
        //
    }

    @Post("/signup")
    @Description("Register user using username and password. Both username and password are case se")
    @Returns(201, { description: "User created", type: UserCreatedResponseModel })
    public async SignUp(
        @Res() response: express.Response,
        @Next() next: express.NextFunction,
        @BodyParams() body: SignUpUserModel): Promise<void> {

            try {
                // Validate user
                let user: UserDocument = ValidateUser(body) as UserDocument;

                // Check if username already taken
                if (await CheckIfUsernameTaken(user.username, this.dependencies.database) === true) {
                    throw new ServiceError("Username already taken", 400, "userNameAlreadyTaken");
                }

                // Hash password
                let hashedPassword: HashedPassword = await HashPassword(user.password);
                user.password = hashedPassword.password;
                user.salt = hashedPassword.salt;
                user.type = "user";

                // Save to database
                let createUser: UserDocument = await this.dependencies.database.createUser(user);
                response.status(201);
                response.json({ id: createUser._id });
            } catch (e) {
                next(e);
            }
    }

    @Post("/login")
    @Description("Login user with username and password")
    @Returns(201, { description: "User created", type: UserCreatedResponseModel })
    public async Login(
        @Res() response: express.Response,
        @Next() next: express.NextFunction,
        @BodyParams() body: LoginRequestModel): Promise<void> {
            try {
                let user: UserDocument = await LoginUser(body.username, body.password, this.dependencies.database);
                let token: AuthToken = GetAuthToken(user, "SecretKey");

                response.json({
                    token: token.token,
                    expires_in: token.expiresIn,
                    type: "Bearer"
                });
            } catch (e) {
                next(e);
            }
    }
}
