
import { JsonProperty, Required } from "@tsed/common";
import { User } from "ts/src/interfaces/User";
import { LoginPayload } from "ts/src/interfaces/LoginPayload";

export class LoginRequestModel implements LoginPayload {
    @Required()
    @JsonProperty()
    public username: string;
    @Required()
    @JsonProperty()
    public password: string;

    constructor(username: string, password: string) {
        this.username = username;
        this.password = password;
    }
}
