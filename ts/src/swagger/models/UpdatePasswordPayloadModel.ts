
import { JsonProperty, Required } from "@tsed/common";
import { UpdatePasswordPayload } from "ts/src/interfaces/UpdatePasswordPayload";

export class UpdatePasswordPayloadModel implements UpdatePasswordPayload {
    @Required()
    @JsonProperty()
    public oldPassword: string;
    @Required()
    @JsonProperty()
    public newPassword: string;

    constructor(oldPassword: string, newPassword: string) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }
}
