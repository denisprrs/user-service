
import { JsonProperty, Required } from "@tsed/common";
import { LikePayload } from "ts/src/interfaces/LikePayload";

export class LikePayloadModel implements LikePayload {
    @Required()
    @JsonProperty()
    public userId: string;

    constructor(userId: string) {
        this.userId = userId;
    }
}
