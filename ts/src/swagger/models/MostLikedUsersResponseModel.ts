
import { JsonProperty, Required } from "@tsed/common";
import { UserMostLiked } from "ts/src/interfaces/UserMostLiked";

export class MostLikedUsersResponseModel implements UserMostLiked {
    @Required()
    @JsonProperty()
    public username: string;
    @Required()
    @JsonProperty()
    public id: string;
    @Required()
    @JsonProperty()
    public likes: number;

    constructor(username: string, id: string, likes: number) {
        this.username = username;
        this.id = id;
        this.likes = likes;
    }
}
