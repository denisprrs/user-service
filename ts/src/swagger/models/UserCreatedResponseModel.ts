
import { JsonProperty, Required } from "@tsed/common";

export class UserCreatedResponseModel {
    @Required()
    @JsonProperty()
    public id: string;

    constructor(id: string) {
        this.id = id;
    }
}
