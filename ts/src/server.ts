import { ServerLoader, IServerSettings } from "@tsed/common";
import * as bodyParser from "body-parser";
import { ErrorHandlerMiddleware } from "./middlewares/ErrorHandlerMiddleware";
import { AddressInfo } from "net";
import ServerSettings from "./interfaces/ServiceSettings";
import { Info, ApiKeySecurity, Spec } from "swagger-schema-official";


const serviceSettings: IServerSettings = {
    acceptMimes: ["application/json", "application/"],
    endpointUrl: "http://localhost",
    httpPort: 3000,
    httpsPort: false,
    routers: {
        mergeParams: true,
        strict: false,
        caseSensitive: false
    },
    swagger: {
        path: "/api-docs",
        spec: {
            swagger: "2.0",
            schemes: ["http"],
            info: {
                version: "1.0.0",
                title: "User Service"
            } as Info,
            paths: undefined,
            security: [{
                Bearer: []
            }],
            securityDefinitions: {
                Bearer: {
                    description: "Security",
                    type: "apiKey",
                    name: "authorization",
                    in: "header"
                } as ApiKeySecurity
            },
            consumes: [
                "application/json"
            ],
            produces: [
                "application/json"
            ],
        },
    }
};

export default class Server extends ServerLoader {
    constructor(ctServerSettings: ServerSettings) {
        super();
        serviceSettings.componentsScan = ctServerSettings.componentsScan;
        serviceSettings.mount = ctServerSettings.mount;
        this.setSettings(serviceSettings);
    }

    public $onMountingMiddlewares(): void {
        this.use(bodyParser.urlencoded({ extended: true }));
        this.use(bodyParser.json());
    }

    public $onReady(): void {
        this.httpServer.keepAliveTimeout = 120000;
    }

    public $onServerInitError(err: Error): void {
        //
    }

    public $afterRoutesInit(): void {
        this.httpServer.on("listening", () => {
            const address: string | AddressInfo = this.httpServer.address();
            let port: number;
            if (typeof address === "string") {
                // tslint:disable-next-line
                console.error("Could not read server port.");
            } else {
                port = address.port;
            }
        });

        this.use(ErrorHandlerMiddleware);
    }
}
