import { Service } from "@tsed/common";
import { Database } from "../database/Database";
import { UserMongoRepository } from "../database/UserMongoRepository";
import { LikeMongoRepository } from "../database/LikeMongoRepository";
const config: any = require("./../../../config/config.json");


@Service()
export default class DependencyService {
    private static instance: DependencyService;
    public database: Database;

    private constructor() {
        if (DependencyService.instance) {
            this.database = DependencyService.instance.database;
        } else {
            DependencyService.instance = this;
            this.database = new Database(
                { uri: config.databaseUri },
                new UserMongoRepository(),
                new LikeMongoRepository()
            );
        }
    }

    static createInstance(): DependencyService {
        if (!this.instance) {
            this.instance = new DependencyService();
        }

        return this.instance;
    }
}
