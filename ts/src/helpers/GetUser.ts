import ServiceError from "../extend/ServiceError";
import DependencyService from "../services/DependecyService";
import { UserDocument } from "../interfaces/UserDocument";
import { UserMe } from "../interfaces/UserMe";

export default async function GetUser(userId: string, dependencies: DependencyService): Promise<UserMe> {
    let user: UserDocument | null = await dependencies.database.getUserById(userId);

    if (user === null) {
        throw new ServiceError("User not found", 404, "userNotFound");
    }
    return {
        username: user.username,
        id: user.id
    };
}
