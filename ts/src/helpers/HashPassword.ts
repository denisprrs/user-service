import { randomBytes, pbkdf2 } from "crypto";
import { HashedPassword } from "../interfaces/HashedPassword";

/**
 * Hashes password so no one can decrypt it
 *
 * @param password String
 * @param salt String
 */
export function HashPassword(password: string, salt?: string): Promise<HashedPassword> {
    return new Promise((resolve: any, reject: any): void => {
        if (salt === undefined) {
            salt = randomBytes(32).toString("hex");
        }

        // This is build in function in crypto module and uses Thread pool for faster execution
        pbkdf2(password, salt, 13542, 512, "sha512", (error: any, key: Buffer) => {
            if (error) {
                return reject(error);
            }

            return resolve({
                salt: salt,
                password: key.toString("hex")
            });
        });
    });
}
