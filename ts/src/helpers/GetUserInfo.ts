import { LikeDocument } from "../interfaces/LikeDocument";
import ServiceError from "../extend/ServiceError";
import DependencyService from "../services/DependecyService";
import { UserDocument } from "../interfaces/UserDocument";
import { PublicUser } from "../interfaces/PublicUser";

export default async function GetUserInfo(userId: string, dependencies: DependencyService): Promise<PublicUser> {
    let user: UserDocument | null = await dependencies.database.getUserById(userId);

    if (user === null) {
        throw new ServiceError("User not found", 404, "userNotFound");
    }

    let likes: LikeDocument[] | null = await dependencies.database.getLikesForUser(userId);

    return {
        username: user.username,
        likes: likes === null ? 0 : likes.length
    };
}
