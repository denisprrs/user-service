import { LikeDocument } from "../interfaces/LikeDocument";
import ServiceError from "../extend/ServiceError";
import DependencyService from "../services/DependecyService";

export default async function UnlikeUser(userId: string, unlikeUserId: string, dependencies: DependencyService): Promise<void> {
    let likedDocument: LikeDocument | null = await dependencies.database.getLike(userId, unlikeUserId);

    if (likedDocument === null) {
        throw new ServiceError("Like not found", 404, "likeNotFound");
    }

    await dependencies.database.deleteLike(likedDocument);
}
