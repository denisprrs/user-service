import { Database } from "../database/Database";
import { UserDocument } from "../interfaces/UserDocument";

export default async function CheckIfUsernameTaken(username: string, database: Database): Promise<boolean> {
    let user: UserDocument | null = await database.getUserByUsername(username);

    if (user === null) {
        return false;
    }

    return true;
}
