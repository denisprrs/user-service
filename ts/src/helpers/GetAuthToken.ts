import { UserDocument } from "../interfaces/UserDocument";
import * as jwt from "jsonwebtoken";
import { AuthToken } from "../interfaces/AuthToken";

/**
 * Create JWT token signed with secret key
 *
 *  !!! In production there should be used private/public key stored in safe location !!!
 *  !!! signed with some kind of cryptographic hash function (e.g. RSA SHA256 algorithm) !!!
 *
 * @param user UserDocument
 */
export default function GetAuthToken(user: UserDocument, secret: string): AuthToken {
    let expiresIn: Date = new Date();
    expiresIn.setHours(expiresIn.getHours() + 1);

    let token: string = jwt.sign(
        {
            iss: "awsome-service",
            sub: user._id,
            iat: Math.floor(Date.now() / 1000) - 30,
            exp: Math.floor(Number(expiresIn.getTime()) / 1000)
        },
        secret
    );

    return {
        token: token,
        expiresIn: Math.floor(Number(expiresIn.getTime()) / 1000) - Math.floor(Date.now() / 1000)
    };
}
