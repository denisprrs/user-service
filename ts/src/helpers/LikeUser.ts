import { LikeDocument } from "../interfaces/LikeDocument";
import ServiceError from "../extend/ServiceError";
import DependencyService from "../services/DependecyService";
import { UserDocument } from "../interfaces/UserDocument";

export default async function LikeUser(userId: string, likedUserId: string, dependencies: DependencyService): Promise<void> {
    if (userId === likedUserId) {
        throw new ServiceError("You obviously cannot like yourself, you narcissist.", 400, "cannotLikeYourSelf");
    }

    let user: UserDocument | null = await dependencies.database.getUserById(likedUserId);

    if (user === null) {
        throw new ServiceError("User not found", 404, "userNotFound");
    }

    let likeDoc: LikeDocument = <LikeDocument>{
        givenBy: userId,
        userId: likedUserId
    };

    try {
        await dependencies.database.createLike(likeDoc);
    } catch (e) {
        if (e.name === "MongoError" && e.message && e.message.includes("duplicate key error collection")) {
            throw new ServiceError("You already liked this user", 400, "alreadyLiked");
        }

        throw e;
    }

}
