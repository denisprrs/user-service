import { UserDocument } from "../interfaces/UserDocument";
import { Database } from "../database/Database";
import ServiceError from "../extend/ServiceError";
import { HashedPassword } from "../interfaces/HashedPassword";
import { HashPassword } from "./HashPassword";

export default async function LoginUser(username: string, password: string, database: Database): Promise<UserDocument> {
    let user: UserDocument | null = await database.getUserByUsername(username);

    if (user === null) {
        throw new ServiceError("User with given username does not exits.", 400, "usernameNotFound");
    }

    let hashedPassword: HashedPassword = await HashPassword(password, user.salt);

    if (hashedPassword.password === user.password) {
        return user;
    }

    throw new ServiceError("Password does not match.", 400, "invalidPassword");
}
