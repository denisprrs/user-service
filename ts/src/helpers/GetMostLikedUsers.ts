import { UserMostLiked } from "../interfaces/UserMostLiked";
import { UserDocument } from "../interfaces/UserDocument";
import { LikeDocument } from "../interfaces/LikeDocument";
import DependencyService from "../services/DependecyService";

export default async function GetMostLikedUsers(dependencies: DependencyService): Promise<UserMostLiked[]> {
    let users: UserDocument[] | null = await dependencies.database.getUsers();

    if (users === null) {
        users = [];
    }

    let likes: LikeDocument[] | null = await dependencies.database.getLikes();

    if (likes === null) {
        likes = [];
    }

    let allUsers: UserMostLiked[] = [];

    for (let user of users) {
        let allLikedForUser: LikeDocument[] = likes.filter((val: LikeDocument) => val.userId === user.id);
        allUsers.push({
            username: user.username,
            id: user.id,
            likes: allLikedForUser.length
        });
    }

    allUsers.sort((a: UserMostLiked, b: UserMostLiked) => {
        return b.likes - a.likes;
    });

    return allUsers;
}
