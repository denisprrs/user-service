import ServiceError from "../extend/ServiceError";
import DependencyService from "../services/DependecyService";
import { UserDocument } from "../interfaces/UserDocument";
import { HashPassword } from "./HashPassword";
import { HashedPassword } from "../interfaces/HashedPassword";

export default async function UpdatePassword(userId: string, oldPassword: string, newPassword: string, dependencies: DependencyService): Promise<void> {
    let user: UserDocument | null = await dependencies.database.getUserById(userId);

    if (user === null) {
        throw new ServiceError("User not found", 404, "userNotFound");
    }

    let hashedPassword: HashedPassword = await HashPassword(oldPassword, user.salt);

    if (hashedPassword.password !== user.password) {
        throw new ServiceError("Old and new password does not match", 400, "passwordsNotMatching");
    }

    let hashedPasswordNew: HashedPassword = await HashPassword(newPassword);
    user.password = hashedPasswordNew.password;
    user.salt = hashedPasswordNew.salt;

    await dependencies.database.updateUser(user);
}
