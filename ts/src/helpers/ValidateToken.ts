import * as jwt from "jsonwebtoken";
import ServiceError from "../extend/ServiceError";
import { Token } from "../interfaces/Token";

/**
 * Create JWT token signed with secret key
 *
 *  !!! In production there should be used private/public key stored in safe location !!!
 *  !!! signed with some kind of cryptographic hash function (e.g. RSA SHA256 algorithm) !!!
 *
 * @param user UserDocument
 */
export default function ValidateToken(token: string, secret: string): Token {
    try {
        let data: Token = jwt.verify(token, secret);
        return data;
    } catch (e) {
        throw new ServiceError("Token is invalid", 401, "invalidToken");
    }
}
