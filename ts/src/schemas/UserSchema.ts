import { Schema, model, Model } from "mongoose";
import { UserDocument } from "../interfaces/UserDocument";

const schema: Schema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    createdAt: {
        type: Number,
        required: false
    },
    modifiedAt: {
        type: Number,
        required: false
    }
});

schema.pre<UserDocument>("save", function(next: any): void {
    let now: number = Math.round(Date.now() / 1000);

    if (!this.createdAt) {
        this.createdAt = now;
    }

    this.modifiedAt = now;
    this.type = "user";

    next();
});

const UserDocumentSchema: Model<UserDocument> = model<UserDocument>("UserDocument", schema);

export default UserDocumentSchema;

