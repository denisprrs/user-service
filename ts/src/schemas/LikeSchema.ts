import { Schema, model, Model } from "mongoose";
import { LikeDocument } from "../interfaces/LikeDocument";

const schema: Schema = new Schema({
    userId: {
        type: String,
        required: true
    },
    givenBy: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    createdAt: {
        type: Number,
        required: false
    }
});

schema.pre<LikeDocument>("save", function(next: any): void {
    let now: number = Math.round(Date.now() / 1000);

    if (!this.createdAt) {
        this.createdAt = now;
    }

    this.type = "like";

    next();
});

schema.index({givenBy: 1, userId: 1}, { unique: true, name: "UniqueUserLike" });

const LikeDocumentSchema: Model<LikeDocument> = model<LikeDocument>("LikeDocument", schema);

export default LikeDocumentSchema;

