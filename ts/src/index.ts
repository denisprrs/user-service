import Server from "./server";
import ServerSettings from "./interfaces/ServiceSettings";
import path = require("path");
import DependencyService from "./services/DependecyService";

const rootDir: string = path.resolve(__dirname);

let serverSettings: ServerSettings = {
    mount: {
        "/": `${rootDir}/controllers/*.js`
    },
    componentsScan: [
        `${rootDir}/services/**/**.js`
    ]
};

let ds: DependencyService = DependencyService.createInstance();

Promise.all([
    ds.database.initialize()
]).then(() => {
    let serviceGlueServer: Server = new Server(serverSettings);
    serviceGlueServer.start();
});


process.on("unhandledRejection", (reason: any, p: any): void => {
        // tslint:disable-next-line
        console.error(reason, "Unhandled Rejection at Promise", p);
        process.exit(1);
    }).on("uncaughtException", (err: any): void => {
        // tslint:disable-next-line
        console.error(err, "Uncaught Exception thrown");
        process.exit(1);
    });
