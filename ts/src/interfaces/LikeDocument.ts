import { Document } from "mongoose";

export interface LikeDocument extends Document {
    userId: string;
    givenBy: string;
    createdAt?: number;
    type?: string;
}
