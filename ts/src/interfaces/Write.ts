import { Types, Document } from "mongoose";

export interface Write<T> {
    create: (item: T) => Promise<T>;
    update: (_id: Types.ObjectId, item: T) => Promise<T>;
    delete: (_id: Types.ObjectId) => Promise<void>;
}
