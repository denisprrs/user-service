
export interface Token {
    iss: string;
    sub: string;
    iat: number;
    exp: number;
}
