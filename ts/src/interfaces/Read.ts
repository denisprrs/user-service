import { DocumentQuery, Document } from "mongoose";

export interface Read<T> {
    retrieveByType: (type: string) => Promise<T[]>;
    find(cond: Object, options: Object): Promise<T[]>;
    /*
    retrieve: () => DocumentQuery<T[], Document>;
    findById: (id: string, callback: (error: any, result: T) => void) => void;
    findOne(cond?: Object, callback?: (err: any, res: T) => void): DocumentQuery<Document | null, Document>;
    find(cond: Object, fields: Object, options: Object, callback?: (err: any, res: T[]) => void): DocumentQuery<Document[], Document>;
    */
}
