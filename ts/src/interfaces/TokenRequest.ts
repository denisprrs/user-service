import { Token } from "./Token";
import * as express from "express";

export interface TokenRequest extends express.Request {
    token: Token;
}
