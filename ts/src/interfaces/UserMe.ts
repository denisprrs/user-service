export interface UserMe {
    username: string;
    id: string;
}
