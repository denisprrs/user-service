
export interface PublicUser {
    username: string;
    likes: number;
}
