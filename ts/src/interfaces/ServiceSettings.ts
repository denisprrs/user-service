import { IServerMountDirectories } from "@tsed/common";

export default interface ServerSettings {
    mount?: IServerMountDirectories;
    componentsScan?: string[];
}
