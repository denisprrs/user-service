import { Document } from "mongoose";
import { User } from "./User";

export interface UserDocument extends User, Document {
    salt: string;
    type?: string;
    createdAt?: number;
    modifiedAt?: number;
}
