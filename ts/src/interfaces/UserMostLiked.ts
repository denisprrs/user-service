export interface UserMostLiked {
    username: string;
    id: string;
    likes: number;
}
