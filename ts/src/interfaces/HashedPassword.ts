
export interface HashedPassword {
    salt: string;
    password: string;
}
