
export interface UpdatePasswordPayload {
    oldPassword: string;
    newPassword: string;
}
