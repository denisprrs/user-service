import { EndpointInfo, EndpointMetadata, IMiddleware, Middleware, Res, Req, Next } from "@tsed/common";
import * as express from "express";
import ServiceError from "../extend/ServiceError";
import ValidateToken from "../helpers/ValidateToken";
import { TokenRequest } from "../interfaces/TokenRequest";
import { Token } from "../interfaces/Token";

/**
 * Authorizes endpoint with given scope. Sets token to request (CTRequest)
 */
@Middleware()
export default class SecurityTokenMiddleware implements IMiddleware {
    use(@Req() req: TokenRequest, @EndpointInfo() endpoint: EndpointMetadata, @Res() response: express.Response, @Next() next: express.NextFunction): void {
        if (req.headers["authorization"] === undefined || typeof req.headers["authorization"] !== "string") {
            next(new ServiceError("Unauthorized access", 401, "unauthorizedAccess"));
            return;
        }

        try {
            let bearer: string[] = req.headers["authorization"]!.split(" ");
            let token: Token = ValidateToken(bearer[1], "SecretKey");
            req.token = token;
            next();
        } catch (e) {
            next(new ServiceError("Unauthorized access", 401, "unauthorizedAccess"));
            return;
        }
    }
}
