import { NextFunction as ExpressNext, Request as ExpressRequest, Response as ExpressResponse } from "express";
import {
    MiddlewareError,
    Request,
    Response,
    Next,
    Err,
    IMiddlewareError
} from "@tsed/common";
import ServiceError from "../extend/ServiceError";

@MiddlewareError()
export class ErrorHandlerMiddleware implements IMiddlewareError {

    use(
        @Err() error: any,
        @Request() request: ExpressRequest,
        @Response() response: ExpressResponse,
        @Next() next: ExpressNext
    ): void {
        if (error instanceof ServiceError) {
            response.status(error.status);
            response.json({
                message: error.message,
                errorCode: error.errorCode,
            });
        } else if (error.status >= 400 && error.status < 500) {
            response.status(error.status);
            response.json({
                message: error.message,
                errorCode: "invalidRequest",
            });
        } else {
            response.status(500);
            response.json({
                message: "Something wet wrong. Please try again."
            });
        }
    }
}
