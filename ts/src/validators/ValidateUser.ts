import { User } from "../interfaces/User";
import * as _ from "lodash";
import ServiceError from "../extend/ServiceError";

export function ValidateUser(user: User): User {
    if (typeof user.password !== "string") {
        throw new ServiceError("Invalid property 'password'", 400, "invalidData");
    }

    if (typeof user.username !== "string") {
        throw new ServiceError("Invalid property 'username'", 400, "invalidData");
    }

    return {
        username: user.username,
        password: user.password
    };
}
