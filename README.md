# User service

This service provides convenient way for users to sign up, sign in and rate other users.

## Installation Prerequisites

There are a few important prerequisites which should be prepared before running:

### MongoDB

MongoDB is a document database which belongs to a family of databases called NoSQL.
You can install one [here](https://docs.mongodb.com/manual/installation/)

### NodeJS

Install at least version 8 of NodeJS. Recommended is LTS. Also npm should be version 6, since its using npm audit.

## Build

Run 
```sh
    npm i
    npm run build
    rm -rf node_modules
    npm i --only=production
    npm run start
```

OR 

Run ```./install.sh```

## Swagger

Navigate to [SwaggerAPI](http://localhost:3000/api-docs/) to see Swagger UI

### Authentication 

When you get token at ```/login``` API, you can then use this token in Swagger UI to make authenticated requests. All you have to do is to click on lock, on the API or on top of the page and insert token in form of ```Bearer <api>``` (note that in order to work, you need to add Bearer type).